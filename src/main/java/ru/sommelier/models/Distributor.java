package ru.sommelier.models;

import java.util.Objects;

public class Distributor {
    private Integer id;
    private String name;

    public Distributor(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Distributor(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Distributor distributor = (Distributor) o;
        return id.equals(distributor.id) &&
                name.equals(distributor.name);
    }

    @Override
    public String toString() {
        return "id:"+ this.id + "; name:"+ this.name;
    }
}
