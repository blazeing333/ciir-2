package ru.sommelier.models;

import java.util.Objects;

public class Place {
    private Integer id;
    private String name;

    public Place(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Place(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Place place = (Place) o;
        return id.equals(place.id) &&
                name.equals(place.name);
    }

    @Override
    public String toString() {
        return "id:"+ this.id + "; name:"+ this.name;
    }
}
