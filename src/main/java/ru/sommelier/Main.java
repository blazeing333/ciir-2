package ru.sommelier;

import ru.sommelier.control.*;
import ru.sommelier.models.User;

import javax.sql.DataSource;
import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Properties properties = new Properties();
        Connection connection;
        try {
            properties.load(new FileReader(new File("D://Doccuments//javaProjects//CIIR//ciir_labs//src//main//resources//dbproperties.properties")));
            String dbUser = properties.getProperty("dbUser");
            String dbUrl = properties.getProperty("dbUrl");
            String dbPassword = properties.getProperty("dbPassword");
            String dbClass = properties.getProperty("dbClassForName");

            Class.forName(dbClass);
            connection = DriverManager.getConnection(dbUrl,dbUser,dbPassword);

            boolean boolForWhile = true;
            while(boolForWhile){
                System.out.println("Select:");
                System.out.println("1. User control.");
                System.out.println("2. Product control.");
                System.out.println("3. Place control.");
                System.out.println("4. Distributor control.");
                System.out.println("5. User-product relations control.");
                System.out.println("6. Place-product relations control.");
                System.out.println("7. Distributor-product relations control.");
                System.out.println("0. Exit.");
                System.out.println("Enter choice:");
                Scanner scanner = new Scanner(System.in);
                Integer choice =  scanner.nextInt();

                switch (choice){
                    case(1):{
                        UserDAO userDAO = new UserDAO(connection);

                        boolean boolForUserWhile = true;
                        while(boolForUserWhile){
                            System.out.println("1. Create new user.");
                            System.out.println("2. Read all users.");
                            System.out.println("3. Update user.");
                            System.out.println("4. Delete user.");
                            System.out.println("0. Exit.");
                            System.out.println("Enter choice:");

                            Scanner userSwitchScanner = new Scanner(System.in);
                            Integer userSwitchChoice = userSwitchScanner.nextInt();

                            switch (userSwitchChoice){
                                case(1):{
                                    userDAO.create();
                                };break;
                                case(2):{
                                    userDAO.readAll();
                                };break;
                                case(3):{
                                    userDAO.update();
                                };break;
                                case(4):{
                                    userDAO.delete();
                                };break;
                                case(0):{
                                    boolForUserWhile = false;
                                }
                            }
                        }
                    };break;
                    case(2):{
                        ProductDAO productDAO = new ProductDAO(connection);

                        boolean boolForProductWhile = true;
                        while(boolForProductWhile){
                            System.out.println("1. Create new product.");
                            System.out.println("2. Read all products.");
                            System.out.println("3. Update product.");
                            System.out.println("4. Delete product.");
                            System.out.println("0. Exit.");
                            System.out.println("Enter choice:");

                            Scanner productSwitchScanner = new Scanner(System.in);
                            Integer productSwitchChoice = productSwitchScanner.nextInt();

                            switch (productSwitchChoice){
                                case(1):{
                                    productDAO.create();
                                };break;
                                case(2):{
                                    productDAO.readAll();
                                };break;
                                case(3):{
                                    productDAO.update();
                                };break;
                                case(4):{
                                    productDAO.delete();
                                };break;
                                case(0):{
                                    boolForProductWhile = false;
                                }
                            }
                        }
                                };break;
                    case(3):{
                        PlaceDAO placeDAO = new PlaceDAO(connection);

                        boolean boolForPlaceWhile = true;
                        while(boolForPlaceWhile){
                            System.out.println("1. Create new place.");
                            System.out.println("2. Read all places.");
                            System.out.println("3. Update place.");
                            System.out.println("4. Delete place.");
                            System.out.println("0. Exit.");
                            System.out.println("Enter choice:");

                            Scanner placeSwitchScanner = new Scanner(System.in);
                            Integer placeSwitchChoice = placeSwitchScanner.nextInt();

                            switch (placeSwitchChoice){
                                case(1):{
                                    placeDAO.create();
                                };break;
                                case(2):{
                                    placeDAO.readAll();
                                };break;
                                case(3):{
                                    placeDAO.update();
                                };break;
                                case(4):{
                                    placeDAO.delete();
                                };break;
                                case(0):{
                                    boolForPlaceWhile = false;
                                }
                            }
                        }
                    };break;
                    case(4):{
                        DistributorDAO distributorDAO = new DistributorDAO(connection);

                        boolean boolForDistributorWhile = true;
                        while(boolForDistributorWhile){
                            System.out.println("1. Create new distributor.");
                            System.out.println("2. Read all distributors.");
                            System.out.println("3. Update distributor.");
                            System.out.println("4. Delete distributor.");
                            System.out.println("0. Exit.");
                            System.out.println("Enter choice:");

                            Scanner distributorSwitchScanner = new Scanner(System.in);
                            Integer distributorSwitchChoice = distributorSwitchScanner.nextInt();

                            switch (distributorSwitchChoice){
                                case(1):{
                                    distributorDAO.create();
                                };break;
                                case(2):{
                                    distributorDAO.readAll();
                                };break;
                                case(3):{
                                    distributorDAO.update();
                                };break;
                                case(4):{
                                    distributorDAO.delete();
                                };break;
                                case(0):{
                                    boolForDistributorWhile = false;
                                }
                            }
                        }
                    };break;
                    case(5):{
                        BasketDAO basketDAO = new BasketDAOImpl(connection);

                        boolean boolForBasketWhile = true;
                        while(boolForBasketWhile){
                            System.out.println("1. Add product to user.");
                            System.out.println("2. Read all user products.");
                            System.out.println("3. Read all product users.");
                            System.out.println("4. Delete product of user.");
                            System.out.println("0. Exit.");
                            System.out.println("Enter choice:");

                            Scanner basketSwitchScanner = new Scanner(System.in);
                            Integer basketSwitchChoice = basketSwitchScanner.nextInt();

                            switch (basketSwitchChoice){
                                case(1):{
                                    basketDAO.createRelation();
                                };break;
                                case(2):{
                                    basketDAO.findUserProductsById();
                                };break;
                                case(3):{
                                    basketDAO.findProductUsersById();
                                };break;
                                case(4):{
                                    basketDAO.deleteRelation();
                                };break;
                                case(0):{
                                    boolForBasketWhile = false;
                                }
                            }
                        }


                    };break;
                    case(6):{
                        PlaceOfSaleDAO placeOfSaleDAO = new PlaceOfSaleDAOImpl(connection);

                        boolean boolForPlaceOfSaleWhile = true;
                        while(boolForPlaceOfSaleWhile){
                            System.out.println("1. Add product to place.");
                            System.out.println("2. Read all place products.");
                            System.out.println("3. Read all product places.");
                            System.out.println("4. Delete product of place.");
                            System.out.println("0. Exit.");
                            System.out.println("Enter choice:");

                            Scanner placeOfSaleSwitchScanner = new Scanner(System.in);
                            Integer placeOfSaleSwitchChoice = placeOfSaleSwitchScanner.nextInt();

                            switch (placeOfSaleSwitchChoice){
                                case(1):{
                                    placeOfSaleDAO.createRelation();
                                };break;
                                case(2):{
                                    placeOfSaleDAO.findPlaceProductsById();
                                };break;
                                case(3):{
                                    placeOfSaleDAO.findProductPlacesById();
                                };break;
                                case(4):{
                                    placeOfSaleDAO.deleteRelation();
                                };break;
                                case(0):{
                                    boolForPlaceOfSaleWhile = false;
                                }
                            }
                        }
                    };break;
                    case(7):{
                        DistributionDAO distributionDAO = new DistributionDAOImpl(connection);

                        boolean boolForDistributionWhile = true;
                        while(boolForDistributionWhile){
                            System.out.println("1. Add product to distributor.");
                            System.out.println("2. Read all distributor products.");
                            System.out.println("3. Read all product distributors.");
                            System.out.println("4. Delete product of distributor.");
                            System.out.println("0. Exit.");
                            System.out.println("Enter choice:");

                            Scanner distributionSwitchScanner = new Scanner(System.in);
                            Integer distributionSwitchChoice = distributionSwitchScanner.nextInt();

                            switch (distributionSwitchChoice){
                                case(1):{
                                    distributionDAO.createRelation();
                                };break;
                                case(2):{
                                    distributionDAO.findDistributorProductsById();
                                };break;
                                case(3):{
                                    distributionDAO.findProductDistributorsById();
                                };break;
                                case(4):{
                                    distributionDAO.deleteRelation();
                                };break;
                                case(0):{
                                    boolForDistributionWhile = false;
                                }
                            }
                        }
                    };break;
                    case(0):{
                        boolForWhile = false;
                    };break;
                }
            }
        }catch (FileNotFoundException e){
            throw new IllegalStateException(e);
        }
        catch (IOException e){
            throw new IllegalStateException(e);
        }
        catch (ClassNotFoundException e){
            throw new IllegalStateException(e);
        }
        catch (SQLException e){
            throw new IllegalStateException(e);
        }


    }
}
