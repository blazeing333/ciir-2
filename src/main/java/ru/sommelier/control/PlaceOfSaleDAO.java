package ru.sommelier.control;

public interface PlaceOfSaleDAO {

    void createRelation();

    void findPlaceProductsById();

    void findProductPlacesById();

    void deleteRelation();

}