package ru.sommelier.control;

import ru.sommelier.models.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ProductDAO implements CrudDAO<Product> {
    private Connection connection;


    public ProductDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void create() {
        try{
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter name of product:");
            String name = scanner.next();

            PreparedStatement statement = connection.prepareStatement("insert into ciir_product(name) values (?)");
            statement.setString(1, name);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public List<Product> readAll() {
        List<Product> products = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from ciir_product");
            while (resultSet.next()){
                Integer id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                Product product = new Product(id,name);
                products.add(product);

                System.out.println(product.toString());
            }

            return products;

        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id for update:");
        Integer id = scanner.nextInt();
        System.out.println("Enter new name of product:");
        String newName =  scanner.next();
        try {
            PreparedStatement statement = connection.prepareStatement("update ciir_product set name = ? where id = ? ");
            statement.setString(1, newName);
            statement.setInt(2, id);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void delete() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id for delete:");
        Integer id = scanner.nextInt();
        try {
//            PreparedStatement basketStatement = connection.prepareStatement("delete from ciir_basket where product_id = ?");
//            PreparedStatement placeOfSaleStatement = connection.prepareStatement("delete from ciir_place_of_sale where product_id = ?");
//            PreparedStatement distributionStatement = connection.prepareStatement("delete from ciir_distribution where product_id = ?");
//            basketStatement.setInt(1,id);
//            placeOfSaleStatement.setInt(1,id);
//            distributionStatement.setInt(1,id);
//            basketStatement.execute();
//            placeOfSaleStatement.execute();
//            distributionStatement.execute();

            PreparedStatement statement = connection.prepareStatement("delete from ciir_product where id = ? ");
            statement.setInt(1, id);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }
    }
}
