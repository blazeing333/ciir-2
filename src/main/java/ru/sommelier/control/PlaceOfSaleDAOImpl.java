package ru.sommelier.control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class PlaceOfSaleDAOImpl implements PlaceOfSaleDAO {
    private Connection connection;

    public PlaceOfSaleDAOImpl(Connection connection) {

        this.connection = connection;

    }

    @Override
    public void createRelation() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of place:");
        Integer placeId = scanner.nextInt();
        System.out.println("Enter id of product:");
        Integer productId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("insert into ciir_place_of_sale(place_id, product_id) values (?,?)");
            statement.setInt(1, placeId);
            statement.setInt(2, productId);
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void findPlaceProductsById() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of place:");
        Integer placeId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("select * from ciir_place_of_sale where place_id = ?");
            statement.setInt(1, placeId);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            System.out.println("Products id:");
            while(resultSet.next()){
                System.out.println(resultSet.getInt("product_id"));
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void findProductPlacesById() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of product:");
        Integer productId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("select * from ciir_place_of_sale where product_id = ?");
            statement.setInt(1, productId);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            System.out.println("Places id:");
            while(resultSet.next()){
                System.out.println(resultSet.getInt("place_id"));
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }


    }

    @Override
    public void deleteRelation(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of place:");
        Integer placeId = scanner.nextInt();
        System.out.println("Enter id of product:");
        Integer productId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("delete from ciir_place_of_sale where place_id = ? and product_id = ?");
            statement.setInt(1, placeId);
            statement.setInt(2, productId);
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

}
