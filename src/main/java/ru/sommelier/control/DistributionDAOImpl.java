package ru.sommelier.control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class DistributionDAOImpl implements DistributionDAO {

    private Connection connection;

    public DistributionDAOImpl(Connection connection) {

        this.connection = connection;

    }

    @Override
    public void createRelation() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of distributor:");
        Integer distributorId = scanner.nextInt();
        System.out.println("Enter id of product:");
        Integer productId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("insert into ciir_distribution(distributor_id, product_id) values (?,?)");
            statement.setInt(1, distributorId);
            statement.setInt(2, productId);
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void findDistributorProductsById() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of distributor:");
        Integer distributorId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("select * from ciir_distribution where distributor_id = ?");
            statement.setInt(1, distributorId);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            System.out.println("Products id:");
            while(resultSet.next()){
                System.out.println(resultSet.getInt("product_id"));
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void findProductDistributorsById() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of product:");
        Integer productId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("select * from ciir_distribution where product_id = ?");
            statement.setInt(1, productId);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            System.out.println("Distributors id:");
            while(resultSet.next()){
                System.out.println(resultSet.getInt("distributor_id"));
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }


    }

    @Override
    public void deleteRelation(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of distributor:");
        Integer distributorId = scanner.nextInt();
        System.out.println("Enter id of product:");
        Integer productId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("delete from ciir_distribution where distributor_id = ? and product_id = ?");
            statement.setInt(1, distributorId);
            statement.setInt(2, productId);
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

}
