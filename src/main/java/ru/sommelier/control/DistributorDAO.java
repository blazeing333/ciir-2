package ru.sommelier.control;

import ru.sommelier.models.Distributor;
import ru.sommelier.models.Place;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DistributorDAO implements CrudDAO<Distributor>{
    private Connection connection;


    public DistributorDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void create() {
        try{
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter name of distributor:");
            String name =  scanner.next();

            PreparedStatement statement = connection.prepareStatement("insert into ciir_distributor(name) values (?)");
            statement.setString(1, name);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public List<Distributor> readAll() {
        List<Distributor> distributors = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from ciir_distributor");
            while (resultSet.next()){
                Integer id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                Distributor distributor = new Distributor(id,name);
                distributors.add(distributor);

                System.out.println(distributor.toString());
            }

            return distributors;

        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id for update:");
        Integer id = scanner.nextInt();
        System.out.println("Enter new name of distributor:");
        String newName =  scanner.next();
        try {
            PreparedStatement statement = connection.prepareStatement("update ciir_distributor set name = ? where id = ? ");
            statement.setString(1, newName);
            statement.setInt(2, id);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void delete() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id for delete:");
        Integer id = scanner.nextInt();
        try {
//            PreparedStatement distributionStatement = connection.prepareStatement("delete from ciir_distribution where distributor_id = ?");
//            distributionStatement.setInt(1,id);
//            distributionStatement.execute();

            PreparedStatement statement = connection.prepareStatement("delete from ciir_distributor where id = ? ");
            statement.setInt(1, id);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }
    }

}
