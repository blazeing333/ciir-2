package ru.sommelier.control;

public interface BasketDAO{

    void createRelation();

    void findUserProductsById();

    void findProductUsersById();

    void deleteRelation();

}
