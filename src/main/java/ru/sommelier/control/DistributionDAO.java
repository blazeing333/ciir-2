package ru.sommelier.control;

public interface DistributionDAO {

    void createRelation();

    void findDistributorProductsById();

    void findProductDistributorsById();

    void deleteRelation();

}
