package ru.sommelier.control;

import java.util.List;

public interface CrudDAO<T> {
    public void create();

    public List<T> readAll();

    public void update();

    public void delete();

}
