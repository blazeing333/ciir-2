package ru.sommelier.control;

import ru.sommelier.models.Place;
import ru.sommelier.models.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PlaceDAO implements CrudDAO<Place> {
    private Connection connection;


    public PlaceDAO(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void create() {
        try{
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter name of place:");
            String name =  scanner.next();

            PreparedStatement statement = connection.prepareStatement("insert into ciir_place(name) values (?)");
            statement.setString(1, name);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public List<Place> readAll() {
        List<Place> places = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from ciir_place");
            while (resultSet.next()){
                Integer id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                Place place = new Place(id,name);
                places.add(place);

                System.out.println(place.toString());
            }

            return places;

        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id for update:");
        Integer id = scanner.nextInt();
        System.out.println("Enter new name of place:");
        String newName =  scanner.next();
        try {
            PreparedStatement statement = connection.prepareStatement("update ciir_place set name = ? where id = ? ");
            statement.setString(1, newName);
            statement.setInt(2, id);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void delete() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id for delete:");
        Integer id = scanner.nextInt();
        try {
//            PreparedStatement placeOfSaleStatement = connection.prepareStatement("delete from ciir_place_of_sale where place_id = ?");
//            placeOfSaleStatement.setInt(1,id);
//            placeOfSaleStatement.execute();

            PreparedStatement statement = connection.prepareStatement("delete from ciir_place where id = ? ");
            statement.setInt(1, id);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }
    }

}
