package ru.sommelier.control;

import org.postgresql.util.PSQLException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class BasketDAOImpl implements BasketDAO {
    private Connection connection;

    public BasketDAOImpl(Connection connection) {

        this.connection = connection;

    }

    @Override
    public void createRelation() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of user:");
        Integer userId = scanner.nextInt();
        System.out.println("Enter id of product:");
        Integer productId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("insert into ciir_basket(user_id, product_id) values (?,?)");
            statement.setInt(1, userId);
            statement.setInt(2, productId);
            statement.execute();
        }catch (PSQLException e){
            throw new IllegalStateException(e);
        }
        catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void findUserProductsById() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of user:");
        Integer userId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("select * from ciir_basket where user_id = ?");
            statement.setInt(1, userId);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            System.out.println("Products id:");
            while(resultSet.next()){
                System.out.println(resultSet.getInt("product_id"));
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void findProductUsersById() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of product:");
        Integer productId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("select * from ciir_basket where product_id = ?");
            statement.setInt(1, productId);
            statement.execute();

            ResultSet resultSet = statement.getResultSet();

            System.out.println("Users id:");
            while(resultSet.next()){
                System.out.println(resultSet.getInt("user_id"));
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }


    }

    @Override
    public void deleteRelation(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id of user:");
        Integer userId = scanner.nextInt();
        System.out.println("Enter id of product:");
        Integer productId = scanner.nextInt();

        try {
            PreparedStatement statement = connection.prepareStatement("delete from ciir_basket where user_id = ? and product_id = ?");
            statement.setInt(1, userId);
            statement.setInt(2, productId);
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
