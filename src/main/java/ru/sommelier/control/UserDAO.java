package ru.sommelier.control;

import ru.sommelier.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UserDAO implements CrudDAO<User> {
    private Connection connection;


    public UserDAO(Connection connection) {

            this.connection = connection;

    }

    @Override
    public void create() {
    try {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter name:");
        String name =  scanner.next();

        PreparedStatement statement = connection.prepareStatement("insert into ciir_user(name) values (?)");
        statement.setString(1, name);
        statement.execute();
    }catch (SQLException e){
        throw new IllegalStateException(e);
    }
    }

    @Override
    public List<User> readAll() {
        List<User> users = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from ciir_user");
            while (resultSet.next()){
                Integer id = resultSet.getInt("id");
                String name = resultSet.getString("name");
                User user = new User(id,name);
                users.add(user);

                System.out.println(user.toString());
            }

            return users;

        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void update() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id for update:");
        Integer id = scanner.nextInt();
        System.out.println("Enter new name:");
        String newName =  scanner.next();
        try {
            PreparedStatement statement = connection.prepareStatement("update ciir_user set name = ? where id = ? ");
            statement.setString(1, newName);
            statement.setInt(2, id);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }

    }

    @Override
    public void delete() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter id for delete:");
        Integer id = scanner.nextInt();
        try {
            PreparedStatement statement = connection.prepareStatement("delete from ciir_user where id = ? ");
            statement.setInt(1, id);
            statement.execute();
        }catch (SQLException e){
            throw new IllegalStateException(e);
        }
    }
}
